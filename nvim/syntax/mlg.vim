sy case match

sy cluster caml contains=Comment,String,camlConstr,camlQual,camlKeyword,camlBraces

sy keyword mlgRule DECLARE PLUGIN
sy keyword mlgRule AS CLASSIFIED COMMAND END EXTEND SIDEFF TACTIC VERNAC
sy keyword mlgType ident contained
sy keyword camlKeyword begin else end fun function if in let match rec then try with val exn type of raise contained
sy keyword camlKeyword open nextgroup=camlModule skipwhite skipnl skipempty

" WIP handle nested brackets properly to remove the hack that
" requires toplevel brackets to begin a line.
sy region mlgCaml start="^{"ms=e end="^}"me=s contains=@caml
" sy region camlBraces contained contains=@caml start="{" end="}"
sy region Comment start="(\*" end="\*)" contains=Comment contained
sy region String start=+"+ end=+"+ skip=+\\"+
sy match camlModule "\<[A-Z][A-Za-z0-9_]\+" contained
sy match camlConstr "\<[A-Z][A-Za-z0-9_]\+" contained
sy match camlQual "\<\%([A-Z][A-Za-z0-9_]\+\.\)\+" contained

runtime syntax/cppo.vim
sy cluster caml add=cppo

hi link camlKeyword Keyword
hi link mlgRule Typedef
hi link mlgType Type
hi link camlConstr Constant
hi link camlQual camlModule
hi link camlModule Identifier

syntax sync fromstart
