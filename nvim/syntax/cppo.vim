" cppo
syn region cppo start="^\_s*#\%(if\|ifdef\|ifndef\|else\|elif\|endif\|define\|undef\|include\|warning\|error\|ext\|endext\)" end="$"

hi link cppo PreProc
